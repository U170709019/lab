

import java.io.IOException;
import java.util.Random;
import java.util.Scanner;

public class GuessNumber {

	public static void main(String[] args) throws IOException {	
		Scanner reader = new Scanner(System.in); //Creates an object to read user input
		Random rand = new Random(); //Creates an object from Random class
		int number =rand.nextInt(100); //generates a number between 0 and 99
		System.out.print("Can you guess it: ");
        while(true){
			System.out.println("Hi! I'm thinking of a number between 0 and 99.Exit with -1");
			int guess = reader.nextInt(); //Read the user input
			if(guess==-1){
				System.out.println("Exit");
				break ;
			}
			else if(guess<0 && guess>100){
				System.out.println("Unaccetable number");
			}
			else if(guess==number){
				System.out.println("Bravo!!");
			}
			else if(guess<number){
				System.out.println("Upper");
			}
			else if(guess>number){
				System.out.println("Lower");
			}
			else{
				System.out.println("Error");
			}
			//reader.close(); //Close the resource before exiting
		}
	}
}
	
	
